%global date    %(date +%%Y%%m%%d)
%global shortcommit SCOMMITHASH 

Name:    git-lab-porcelain 
Version: 0 
Release: %{date}git%{shortcommit}%{?dist}
Summary: Git porcelain for working with gitlab 
BuildArch: noarch

Requires: python-gitlab
Requires: GitPython
Requires: python-pycurl
Requires: python-tabulate
Requires: curl

License: GPLv2
URL:     https://gitlab.com/nhorman/git-lab-porcelain 

Source0: %{name}-%{shortcommit}.tar.gz

%description
A porcelain for git to facilitate command line creation/listing/editing and
reviewing of merge requests in gitlab

%prep
%autosetup -n git-lab-porcelain-%{shortcommit}


%build
# Convert to unversioned python, if python3 isn't available
sed -i -e"s/\#\!\/usr\/bin\/python3/\#\!\/usr\/bin\/python/" ./git-lab

%install
mkdir -p $RPM_BUILD_ROOT/%{_bindir}/
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1/
install -m 0755 git-lab $RPM_BUILD_ROOT/%{_bindir}/git-lab
install -m 0644 git-lab.1 $RPM_BUILD_ROOT/%{_mandir}/man1/git-lab.1

%files
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Tue Oct 22 2019 Neil Horman <nhorman@redhat.com> - 0-1
- Initial release
